package com.example.storagepoc.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.storagepoc.AppExecutor;
import com.example.storagepoc.R;
import com.example.storagepoc.adapter.HorizontalRecyclerView;
import com.example.storagepoc.utils.FileUtils;
import com.example.storagepoc.utils.ImageUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = "MainActivity";

    private static final int REQUEST_IMAGE_CAPTURE = 90;
    private static final int REQUEST_STORAGE_PERMISSION = 91;
    private static final int SELECT_IMAGE_FROM_GALLERY = 99;
    private static final String FILE_PROVIDER_AUTHORITY = "com.example.storagepoc.fileprovider";

    private AppExecutor mAppExcutor;

    private ImageView mImageView;

    private Button mStartCamera;

    private String mTempPhotoPath;

    private Bitmap mResultsBitmap;

    private Button mClear, mSave, mImport;

    private RecyclerView mImgRecyclerView;

    private List<String> imagesList;

    private ArrayList<Uri> urisList = new ArrayList<>();

    private String selectedImagePath = "";

    private HorizontalRecyclerView horizontalRecyclerView;

    private String mimeType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAppExcutor = new AppExecutor();

        mImageView = findViewById(R.id.imageView);
        mClear = findViewById(R.id.clear);
        mSave = findViewById(R.id.Save);
        mImport = findViewById(R.id.gallary);
        mStartCamera = findViewById(R.id.startCamera);
        mImgRecyclerView = findViewById(R.id.imgRecycler);

        mImageView.setVisibility(View.GONE);
        mImport.setVisibility(View.GONE);
        mSave.setVisibility(View.GONE);
        mClear.setVisibility(View.GONE);

        setClickListeners();
        setAdapter();
    }

    private void setClickListeners() {

        mStartCamera.setOnClickListener(v -> {
            // Check for the external storage permission
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // If you do not have permission, request it
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_STORAGE_PERMISSION);
            } else {
                // Launch the camera if the permission exists
                launchCamera();
            }
        });

        mSave.setOnClickListener((View v) -> {
            mAppExcutor.diskIO().execute(() -> {
                // Delete the temporary image file
                ImageUtils.deleteImageFile(this, mTempPhotoPath);
                // Save the image
                ImageUtils.saveImage(this, mResultsBitmap);
            });
            Toast.makeText(this, "Image Saved", Toast.LENGTH_LONG).show();

        });

        mClear.setOnClickListener(v -> {
            // Clear the image and toggle the view visibility
            mImageView.setImageResource(0);
            mStartCamera.setVisibility(View.VISIBLE);
            mSave.setVisibility(View.GONE);
            mImport.setVisibility(View.GONE);
            mClear.setVisibility(View.GONE);

            mAppExcutor.diskIO().execute(() -> {
                // Delete the temporary image file
                ImageUtils.deleteImageFile(this, mTempPhotoPath);
            });

        });

        mImport.setOnClickListener((View v) -> {
            if (isStoragePermissionGranted()) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                startActivityForResult(intent, SELECT_IMAGE_FROM_GALLERY);
            } else {

            }

        });
    }

    private void setAdapter() {
        horizontalRecyclerView = new HorizontalRecyclerView(urisList, mimeType, MainActivity.this);
        mImgRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mImgRecyclerView.setAdapter(horizontalRecyclerView);

        horizontalRecyclerView.setCloseClickListener(new HorizontalRecyclerView.OnCloseItemClickListener() {
            @Override
            public void onCloseClickListener(Uri uri, int position) {
                horizontalRecyclerView.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Called when you request permission to read and write to external storage
        switch (requestCode) {
            case REQUEST_STORAGE_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // If you get permission, launch the camera
                    launchCamera();
                } else {
                    // If you do not get permission, show a Toast
                    Toast.makeText(this, R.string.permission_denied, Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_IMAGE_CAPTURE:
                    // Process the image and set it to the TextView
                    processAndSetImage();
                    break;
                case SELECT_IMAGE_FROM_GALLERY:
                    if (data.getClipData() != null) {
                        imagesList = new ArrayList<>();
                        int count = data.getClipData().getItemCount();

                        for (int i = 0; i < count; i++) {
                            if (urisList.size() >= 5) {
                                Toast.makeText(this, "You can't select more than 5 pictures.", Toast.LENGTH_SHORT).show();
                            } else {
                                urisList.add(data.getClipData().getItemAt(i).getUri());
                                selectedImagePath = FileUtils.getPathFromUri(this, data.getClipData().getItemAt(i).getUri());
                                imagesList.add(selectedImagePath);
                            }
                            mImgRecyclerView.setVisibility(View.VISIBLE);
                            mImageView.setVisibility(View.GONE);
                            mImport.setVisibility(View.GONE);
                            mSave.setVisibility(View.GONE);
                            mClear.setVisibility(View.GONE);
                        }
                        horizontalRecyclerView.notifyDataSetChanged();
                        break;
                    }

            }
        }
    }


    private void launchCamera() {

        // Create the capture image intent
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the temporary File where the photo should go
            File photoFile = null;
            try {
                photoFile = ImageUtils.createTempImageFile(this);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                // Get the path of the temporary file
                mTempPhotoPath = photoFile.getAbsolutePath();

                // Get the content URI for the image file
                Uri photoURI = FileProvider.getUriForFile(this,
                        FILE_PROVIDER_AUTHORITY,
                        photoFile);
                // Add the URI so the camera can store the image
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                // Launch the camera activity
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    /**
     * Method for processing the captured image and setting it to the TextView.
     */
    private void processAndSetImage() {
        // Toggle Visibility of the views
        mStartCamera.setVisibility(View.GONE);
        mSave.setVisibility(View.VISIBLE);
        mImport.setVisibility(View.VISIBLE);
        mClear.setVisibility(View.VISIBLE);
        mImageView.setVisibility(View.VISIBLE);
        mImgRecyclerView.setVisibility(View.GONE);

        mResultsBitmap = ImageUtils.resamplePic(this, mTempPhotoPath);
        // Set the new bitmap to the ImageView
        mImageView.setImageBitmap(mResultsBitmap);
    }

    private boolean isStoragePermissionGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }
}