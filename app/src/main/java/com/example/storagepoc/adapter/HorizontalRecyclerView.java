package com.example.storagepoc.adapter;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.storagepoc.R;

import java.util.ArrayList;

public class HorizontalRecyclerView  extends RecyclerView.Adapter<HorizontalRecyclerView.HorizontalViewHolder> {

    private static final String TAG="HorizontalRecyclerView";
    private ArrayList<Uri> uri;
    private String contentType = "";
    Context context;
    OnCloseItemClickListener closeItemClickListener;

    public HorizontalRecyclerView(ArrayList<Uri> uri, String feedContentType, Context mContext) {
        this.context = mContext;
        this.uri = uri;
        this.contentType = feedContentType;
    }

    @NonNull
    @Override
    public HorizontalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_images, parent, false);
        return new HorizontalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HorizontalViewHolder horizontalViewHolder, int position) {
        try {
            ContentResolver cR = context.getContentResolver();
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            contentType = mime.getExtensionFromMimeType(cR.getType(uri.get(position)));

            Log.i(TAG, "contenta--> " + contentType);

            if (contentType.equalsIgnoreCase("jpg") || contentType.equalsIgnoreCase("jpeg") || contentType.equalsIgnoreCase("png")) {
                horizontalViewHolder.mImageRecyclerView.setVisibility(View.VISIBLE);
                horizontalViewHolder.mImageRecyclerView.setImageURI(uri.get(position));
            }
        } catch (Exception e) {
            Log.e(TAG,"Exception--> "+e.toString());
        }

    }

    @Override
    public int getItemCount() {
        return uri.size();
    }

    public class HorizontalViewHolder extends RecyclerView.ViewHolder {

        AppCompatImageView mImageRecyclerView;
        LinearLayout btnClose;

        public HorizontalViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageRecyclerView = itemView.findViewById(R.id.img_feed);
            btnClose = itemView.findViewById(R.id.btnClose);

            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        int pos = getAdapterPosition();
                        if (closeItemClickListener != null)
                            if (pos >= 0) {
                                removeAt(pos);
                                closeItemClickListener.onCloseClickListener(uri.get(pos), pos);
                            }
                    } catch (Exception e) {
                        Log.e(TAG,"Exception--> "+e.toString());
                    }
                }
            });
        }
    }

    public void setCloseClickListener(OnCloseItemClickListener itemClickListener) {
        this.closeItemClickListener = itemClickListener;
    }

    public interface OnCloseItemClickListener {
        void onCloseClickListener(Uri uri, int position);
    }

    public void removeAt(int position) {
        uri.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, uri.size());
    }
}
